#ifndef DOWNLOADUNPACK_H
#define DOWNLOADUNPACK_H

#include "downloadunpack_global.h"
//#include "ui_downloadunpack.h"

#include <QMainWindow>
#include <QNetworkReply>
#include <QFile>
#include <QProcess>
#include <QFileInfo>
#include <QTimer>
#include <QFileDialog>
#include <QSettings>
#include <QFontDatabase>
#include <QMessageBox>
#include <QScreen>
#include <QStandardPaths>
#include <QDesktopServices>
#include <QCloseEvent>
#include <QTranslator>
#include <QDebug>


#define DISPLAY_NAME "streamCapture2"
#define DOWNLOADUNPACK_EXECUTABLE_NAME "downloadunpack"
#define EXECUTABLE_NAME2 "streamcapture2"

//#define RED 218
//#define GREEN 255
//#define BLUE 221

#ifdef Q_OS_LINUX
#define DOWNLOADUNPACK_FONT_SIZE 12
#endif
#ifdef Q_OS_WIN
#define DOWNLOADUNPACK_FONT_SIZE 10
#endif

QT_BEGIN_NAMESPACE
namespace Ui
{
class DownloadUnpack;
}
QT_END_NAMESPACE
#ifdef Q_OS_WINDOWS
#ifndef QT_STATIC
class DOWNLOADUNPACK_EXPORT DownloadUnpack : public QMainWindow
#endif

#ifdef QT_STATIC
    class DownloadUnpack : public QMainWindow
#endif
#endif
#ifdef Q_OS_LINUX
        class DownloadUnpack : public QMainWindow
#endif
        {
            Q_OBJECT


        private:
            Ui::DownloadUnpack *ui;
            QString selectedsvtplaydl;
            QTimer *timer;
            QString networkErrorMessages(QNetworkReply::NetworkError error);
            void startConfig();
            QString currentOs();



        public:
            DownloadUnpack(QWidget *parent = nullptr);
            ~DownloadUnpack();
            void endConfig();
            void closeEvent(QCloseEvent *e);



        private slots:
            void directoryListing();
            void download();
            void klocka();
            QString fileDialog();
        };
#endif // DOWNLOADUNPACK_H
