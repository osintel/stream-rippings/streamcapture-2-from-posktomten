#  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#
#    		streamCapture2
#    		Copyright (C) 2016 - 2021 Ingemar Ceicer
#    		https://gitlab.com/posktomten/streamcapture2/
#    		ic_0002 (at) ceicer (dot) com
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
# <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#-------------------------------------------------
#
# Project created by QtCreator 2017-05-07T13:14:14
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = streamcapture2
TEMPLATE = app
#CONFIG += optimize_full
#CONFIG += static

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
# DEFINES += QT_DEPRECATED_WARNINGS
# CONFIG  += cmdline precompile_header
# DEFINES += QT_DISABLE_DEPRECATED_BEFORE
# DEFINES += QT_NO_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#CONFIG += C++11
#CONFIG += /opt/Qt5.15.0_systemfreetype_static/bin/lrelease embed_translations
#CONFIG += C:\Qt\Qt\5.15.0\mingw81_32\bin\lrelease embed_translations

#CONFIG += /opt/Qt5.15.0_systemfreetype_static/bin/lrelease embed_translations
# CONFIG += lrelease embed_translations

# Input
#PRECOMPILED_HEADER  = checkupdate.h \
#                      newprg.h \
#                      info.h \
#		       selectfont.h
# Use Precompiled headers (PCH)
PRECOMPILED_HEADER  = newprg.h
HEADERS += newprg.h \
           info.h


FORMS +=   newprg.ui

SOURCES += \
           coppytodefaultlocation.cpp \
           download.cpp \
           downloadall.cpp \
           downloadallepisodes.cpp \
           downloadfromceicer.cpp \
           drag_and_drop.cpp \
           listallepisodes.cpp \
           newprg.cpp main.cpp \
           about.cpp \
           info.cpp \
           paytv_create.cpp \
           paytv_edit.cpp \
           qsystemtrayicon.cpp \
           save.cpp \
           setgetconfig.cpp \
           language.cpp \
           sok.cpp \
           st_create.cpp \
           st_edit.cpp \
           test_translation.cpp \
#           tesstsearch
#           testsearch.cpp \
           firstrun.cpp \
           shortcuts.cpp \
    zoom.cpp

unix:LIBS += "-L../lib"
unix:LIBS += -lcheckupdate
unix:LIBS += -lupdateappimage
unix:LIBS += -lselectfont
unix:LIBS += -ldownloadunpack
unix:INCLUDEPATH += "../include"


RESOURCES += myres.qrc \
             fonts_forall.qrc

RC_FILE = myapp.rc

TRANSLATIONS += i18n/_streamcapture2_template_xx_XX.ts \
                i18n/_streamcapture2_sv_SE.ts \
                i18n/_streamcapture2_de_DE.ts \
                i18n/_streamcapture2_it_IT.ts

CODECFORSRC = UTF-8
DESTDIR="../build-executable"
UI_DIR = ../code
win32:RC_ICONS += images/icon.ico

win32:CONFIG(release, debug|release): LIBS += -L../lib/ -lcheckupdate  # Release
else:win32:CONFIG(debug, debug|release): LIBS += -L../lib/ -lcheckupdated # Debug
win32:CONFIG(release, debug|release): LIBS += -L../lib/ -lselectfont  # Release
else:win32:CONFIG(debug, debug|release): LIBS += -L../lib/ -lselectfontd # Debug
win32:CONFIG(release, debug|release): LIBS += -L../lib/ -ldownloadunpack  # Release
else:win32:CONFIG(debug, debug|release): LIBS += -L../lib/ -ldownloadunpackd # Debug



INCLUDEPATH += "../include"
DEPENDPATH += "../include"
