16:30 28 Juni 2021
Version 0.29.1
En större omarbetning av programmet, det fungerar bättre att välja
nedladdningskvalitet och alla meddelanden från programmet
är tydligare.
Manualen är omarbetad.
Möjlighet att zooma in- och ut med ett tangentbordstryck.
Uppdaterad svtplay-dl senaste snapshot 4.0-2-g09843d6
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

20:30 25 Juni 2021
Version 0.29.0 BETA 3
En större omarbetning av programmet, det ska fungera bättre att välja
nedladdningskvalitet och alla meddelanden från programmet
ska vara tydliga.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

17:00 25 Juni 2021
Version 0.29.0 BETA 2
En större omarbetning av programmet, det ska fungera bättre att välja
nedladdningskvalitet och alla meddelanden från programmet
ska vara tydliga.
Uppdaterad svtplay-dl senaste snapshot 4.0-2-g09843d6
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

03:00 21 Juni 2021
Version 0.29.0 BETA 1
En större omarbetning av programmet, det ska fungera bättre att välja
nedladdningskvalitet och alla meddelanden från programmet
ska vara tydliga.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

20:00 20 Juni 2021
Version 0.28.15
Viktig bugfix: Nedladdningslistan fungerar nu.
Fungerar inte tidigare om man använde knappen "Lägg till i nedladdningslistan".
Menyn fungerade.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

15:00 20 Juni 2021
Version 0.28.14
Kraftigt förbättrad hastighet på dekomprimeringen av nedladdade
svtplay-dl.zip (Windows).
Linuxversionen kan dekomprimera *.tar.gz och *.zip
Windowsversionen kan dekomprimera *.tar.gz och *.zip
Förbättrade dialogrutor för att spara mediafiler och för
att välja nedladdningsplats för svtplay-dl.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

20:00 19 Juni 2021
Version 0.28.13 BETA 3
Kraftigt förbättrad hastighet på dekomprimeringen av nedladdade
svtplay-dl.zip (Windows).
Linuxversionen kan dekomprimera *.tar.gz och *.zip
Windowsversionen kan dekomprimera *.tar.gz och *.zip
Förbättrade dialogrutor för att spara mediafiler och för
att välja nedladdningsplats för svtplay-dl.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

17:35 18 Juni 2021
Version 0.28.12
Uppdaterad svtplay-dl stabila version 4.0
Uppdaterad svtplay-dl senaste snapshot 4.0
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

23:30 15 Juni 2021
Version 0.28.11
"download svtplay-dl från bin.ceicer.com" delar språkinställningarna
med huvudprogrammet.
Mindre förbättringar.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

19:00 14 Juni 2021
Version 0.28.10 BETA
"download svtplay-dl från bin.ceicer.com" delar språkinställningarna
med huvudprogrammet.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

23:59 13 Juni 2021
Version 0.28.9
Uppdaterad svtplay-dl senaste snapshot 3.9.1-2-g00e5c84
Underhållsverktyget startar med "Update components" (Windows).
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

23:45 12 Juni 2021
Version 0.28.8
Snyggare messagebox vid uppdatering.
Flyttat konfigurationsfilen för "ladda ner svtplay-dl".
Besök källkod och binära filer.
Fullständig italiensk översättning.
Uppdaterad italiensk hjälpfil.
Mindre förbättringar.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

10:30 11 Juni 2021
Version 0.28.7 BETA
Snyggare messagebox vid uppdatering.
Flyttat konfigurationsfilen för "ladda ner svtplay-dl".
Besök källkod och binära filer.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

00:30 10 Juni 2021
Version 0.28.6
Bug fix: Det gick inte att ladda ner. Har tagit bort "--remux"
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

21:30 7 Juni 2021
Version 0.28.5
Uppdaterad svtplay-dl stabila version 3.9.1
Uppdaterad svtplay-dl senaste snapshot 3.9.1
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

18:30 4 Juni 2021
Version 0.28.4
Uppdaterad svtplay-dl senaste snapshot 3.8-27-gd32bc02
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

22:14 3 Juni 2021
Version 0.28.3
Uppdaterad svtplay-dl senaste snapshot 3.8-25-g88432ed
"--list-quality" (sökfunktionen) uppdaterad.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

19:30 30 Maj 2021
Version 0.28.2
Uppdaterad svtplay-dl senaste snapshot 3.8-23-g89284c9
Lagt till "Välj automatiskt" alternativ för bithastighet och metod.
"--list-quality" (sökfunktionen) ytterligare uppdaterad.
Möjlighet att testa språkfiler i "svtplay-dl från bin.ceicer.com"
Fullständig italiensk översättning.
Uppdaterade hjälpfiler, engelska, italienska och svenska.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

01:48 28 Maj 2021
Version 0.28.0
"--list-quality" (sökfunktionen) uppdaterad.
Fungerar med senaste svtplay-dl.
Nyhet: Ladda ner svtplay-dl från streamCapture2.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

21:48 24 Maj 2021
Version 0.27.6
Uppdaterad svtplay-dl senaste snapshot 3.8-22-g45fceaa
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

22:48 23 Maj 2021
Version 0.27.5
Uppdaterad svtplay-dl senaste snapshot 3.8-18-g1c3d8f1
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

17:30 20 Maj 2021
Version 0.27.4
Uppdaterad svtplay-dl senaste snapshot 3.8-13-g3d46e85
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

19:30 17 Maj 2021
Version 0.27.3
Komplett italiensk översättning.
Uppdaterad svtplay-dl senaste snapshot 3.8-10-gc9a606d
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

14:05 16 Maj 2021
Version 0.27.2
'st' cookie fungerar med nedladdningslistan.
Uppdaterad svtplay-dl senaste snapshot 3.8-3-g107d3e0
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

05:05 16 Maj 2021
Version 0.27.1
Bug Fix: Gick inte att söka efter videoströmmar när det krävdes inloggning.
Hantera många olika 'st' cookies.
Redigera konfigurationsfilen.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

17:55 14 Maj 2021
Version 0.26.7
Uppdaterad svtplay-dl senaste snapshot 3.8-2-g2c5101a
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

20:55 13 Maj 2021
Version 0.26.6
Uppdaterad svtplay-dl stabila version 3.8
Uppdaterad svtplay-dl senaste snapshot 3.8
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

15:55 13 Maj 2021
Version 0.26.5
Uppdaterad svtplay-dl senaste snapshot 3.7-12-ga5e4166
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

23:55 11 Maj 2021
Version 0.26.4
Uppdaterad svtplay-dl senaste snapshot 3.7-12-ga5e4166
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

23:55 10 Maj 2021
Version 0.26.3
Uppdaterad svtplay-dl senaste snapshot 3.7-10-gd575112
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

11:05 9 Maj 2021
Version 0.26.2
Uppdaterad svtplay-dl senaste snapshot 3.7-4-g3497e05
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux)

14:05 6 Maj 2021
Version 0.26.1
Möjligt att använda 'st' cookies.
Optimerad, effektivare kod.
Uppdaterad svtplay-dl stabila version 3.7
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

12:45 28 April 2021
Version 0.25.8
Uppdaterad svtplay-dl senaste snapshot 3.7
Förbättrad visning av nedladdningsbara videoströmmar.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

21:00 22 April 2021
Version 0.25.6
Bugg rättad: Det gick inte att söka efter videoströmmar.
Uppdaterad svtplay-dl senaste snapshot 3.6-3-gaae55fc
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

19:00 20 April 2021
Version 0.25.5
Uppdaterad svtplay-dl stabila version 3.6
Uppdaterad svtplay-dl senaste snapshot 3.6-2-g0dcb01f
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

10:00 15 April 2021
Version 0.25.4
Uppdaterad svtplay-dl stabila version 3.5
Uppdaterad svtplay-dl senaste snapshot 3.5-1-g8805627
Uppdaterad FFmpeg 4.4
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

23:59 13 April 2021
Version 0.25.3
Uppdaterad svtplay-dl stabila version 3.4
Ändringar i koden gör programmet en aning snabbare och något mindre.
Möjligt att redigera namnet på strömningstjänster.
Möjligt att ha mellanslag i namnet på strömningstjänster.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

00:30 8 April 2021
Version 0.25.1
Bugg rättad: Fel när man öppnar en egen språkfil i programmet.
Snyggare presentation av "Metod" och "Bithastighet".
Hjälpen översatt till italienska.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

12:31 3 April 2021
Version 0.25.0
Uppdaterad svtplay-dl senaste snapshot 3.3-2-gcdf394e
Namnbyte på "Betal TV" till "Inloggning"
"Direktnedladdning av alla videoströmmar i nuvarande serie (inte från nedladdningslistan)" visar när varje fil blivit nedladdad och hur många filer som återstår.
Förbättrad förloppsindikator.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

08:31 17 Mars 2021
Version 0.24.0
Uppdaterad svtplay-dl stabila version 3.3
Uppdaterad svtplay-dl senaste snapshot 3.3
Förloppsindikator som visar nedladdningarna.
Buggrättning: Nu syns antalet sökningar som du tidigare valt att spara.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

10:00 16 Mars 2021
Version 0.23.2
Uppdaterad svtplay-dl senaste snapshot 3.2-10-g7440e3f
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

12:05 15 Mars 2021
Version 0.23.1
Uppdaterad manual.
Uppdaterad svtplay-dl senaste snapshot 3.2-9-g028971b
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

03:30 13 Mars 2021
Version 0.23.0
Buggfix nedladdning från Viafree.
Dra- och släpp-funktionalitet har lagts till.
Fler meddelanden i statusbaren.
Bättre översättningar i Linux AppImage.
Uppdaterad svtplay-dl stabila version 3.2
Uppdaterad svtplay-dl senaste snapshot 3.2-2-g6ef5d61
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

21:30 6 Mars 2021
Version 0.22.4
Uppdaterad svtplay-dl senaste snapshot 3.1-5-gcb3612b
Linux AppImage uppdaterad till GLIBC 2.27 (2018-02-01). Fungerar nu med de flesta distributioner från maj 2018 och nyare.
Tydligare meddelanden om man försöker kopiera till förvald folder och filen redan finns.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

00:30 1 Mars 2021
Version 0.22.3
Uppdaterad svtplay-dl senaste snapshot 3.1-4-g998f51f (Windows).
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux).

12:00 27 February 2021
Version 0.22.2
"Uppdatera" är inaktiverad om det inte finns någon uppdatering (Linux).
Mindre korrigeringar.
Uppdaterat grafiskt gränssnitt.
Uppdaterad FFmpeg 4.3.2 (Linux).
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux).

00:00 26 February 2021
Version 0.22.1
Uppdaterad svtplay-dl stabila version 3.0-7-ge423c1c (Linux).
Uppdaterad svtplay-dl stabila version 3.1 (Windows).
Uppdaterad svtplay-dl senaste snapshot 3.1 (Linux, Windows).
Uppdaterad FFmpeg 4.3.2
Uppdaterat grafiskt gränssnitt.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux).

00:50 18 Februari 2021
Version 0.22.0
Uppdaterad svtplay-dl senaste snapshot 3.0-5-g8cd5793
Italiensk översättning.
Tagit bort ofullständig tysk översättning.
Möjlighet att testa språkfiler.
"Om" visar en fungerande e-postadress.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux).

00:50 15 Februari 2021
Version 0.21.2
Uppdaterad svtplay-dl stabila version 2.9-8-ga263a66
Uppdaterad svtplay-dl senaste snapshot 3.0
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux).

12:50 25 Januari 2021
Version 0.21.1
AppImage: "Ta bort alla inställningsfiler och avsluta"
Tar bort genvägar på skrivbordet och i programmenyn.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux).

22:50 24 Januari 2021
Version 0.21.0
Uppdaterad svtplay-dl senaste snapshot 2.8-11-gd193679
AppImage: Desktop fil och ikon på skrivbordet
AppImage: Desktop fil och ikon på programmenyn
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux).

22:50 4 Januari 2021
Version 0.20.18
Uppdaterad svtplay-dl senaste snapshot 2.8-9-gc3ff052
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux). 

21:02 9 December 2020
Version 0.20.17 
Uppdaterad svtplay-dl stabila version 2.8-3-g18a8758
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux). 

19:02 21 November 2020
Version 0.20.16 
Uppdaterad svtplay-dl stabila version 2.8
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux). 

18:01 15 November 2020
Version 0.20.15
Uppdaterad svtplay-dl version 2.7-5-gbf1f9c5
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux). 

21:55 6 November 2020
Version 0.20.14
Uppdaterad svtplay-dl version 2.7-4-gef557cb
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux). 

03:45 1 November 2020
Version 0.20.13
Varning för för hög bitrate vid misslyckad nedladdning.
Tillförlitligare uppdatering av AppImage.
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux). 

18:34 27 October 2020
Version 0.20.12
Buggfix: svtplay-dl i systemsökvägen hittades inte.
Uppdaterad svtplay-dl version  2.7
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux). 

18:09 13 Oktober 2020
Version 0.20.11
Uppdaterad svtplay-dl version 2.7-3-ga550bd1
Tillförlitligare uppdatering av AppImage.
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux).

18:09 11 Oktober 2020
Version 0.20.10
Uppdaterad svtplay-dl version 2.6-4-gd6dc139
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux).

20:09 2 Oktober 2020
Version 0.20.9
Uppdaterad svtplay-dl version 2.6
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux).

22:05 27 September 2020
Version 0.20.8
Uppdaterad svtplay-dl version 2.5-7-g7db9f7e
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux).

19:39 23 September 2020
Version 0.20.7
Uppdaterad svtplay-dl svtplay-dl stable version 2.5
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux).

19:39 22 September 2020
Version 0.20.6
Uppdaterad svtplay-dl version 2.5-4-ge92ebbb
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux).

00:55 20 September 2020
Version 0.20.5
Uppdaterad svtplay-dl svtplay-dl version 2.5
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux).

00:43 19 September 2020
Version 0.20.4
Uppdaterad svtplay-dl svtplay-dl version 2.4-71-g5fc7750
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux).

19:43 14 September 2020
Version 0.20.3
Buggfix: Fungerar bättre med senaste versionerna av svtplay-dl
Uppdaterad svtplay-dl version 2.4-67-g478fd3b
Uppdaterad FFmpeg till version 4.3.1 (Linux)
Använder Qt 5.15.0, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.0, kompilerad med GCC 5.4.0 (Linux).

18:43 13 September 2020
Version 0.20.2
Möjlighet att skape genvägar.
Programmet uppdaterar sig självt. (Gäller Linux AppImage.)
Uppdaterad svtplay-dl version 2.4-62-g2920bb9
Använder Qt 5.15.0, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.0, kompilerad med GCC 5.4.0 (Linux).

15:20 12 Juli 2020
Version 0.20.1
Möjlighet att skape en genväg på skrivbordet.
Möjlighet att få aviseringar när nedladdning är klar.
Buggfix: Programmet kunde försvinna om man växklar mellan olika antal skärmar.
Buggfix: Svensk översättning av "FFmpeg can be found in system path" har rättats.
Mindre förbättringar.
Använder Qt 5.15.0, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.0, kompilerad med GCC 5.4.0 (Linux).

13:26 28 Juni 2020
Version 0.20.0
Möjlighet att välja svtplay-dl från systemsökvägen.
Möjlighet att välja svtplay-dl från filsystemet.
Buggfix: Nu kommer streamCapture2 ihåg valt teckensnitt.
Mindre förbättringar.
Använder Qt 5.15.0, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.0, kompilerad med GCC 5.4.0 (Linux).

21:43 15 Juni 2020
Version 0.19.2
Buggrättning: Otrevlig bugg som fick programmet att krascha.
Detta hände när programmet sökte efter uppdateringar vid programstart.
Använder Qt 5.15.0, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.0, kompilerad med GCC 5.4.0 (Linux).

17:07 14 Juni 2020
Version 0.19.1
Använder den inkluderade FFmpeg eller, om den inte finns, systemets FFmpeg.
Använder Qt 5.15.0, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.0, kompilerad med GCC 5.4.0 (Linux).

23:28 3 May 2020
Version 0.19.0
Uppdaterad svtplay-dl version 2.4-42-g916e199
Möjligt att växla mellan svtplay-dl stable och senaste snapshot.
Uppdaterad FFmpeg version 4.2.3
Enkel uppdatering med minimal nedladdmimg.
Använder Qt 5.15.0, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.0, kompilerad med GCC 5.4.0 (Linux).

20:27 27 April 2020
Version 0.18.5
Bugfix: Genvägen på startmenyn fungerade inte i version 0.18.4 (Windows).
Länk till svtplay-dl forum för problem.
Valbart teckensnitt.
Bättre kontrasterande färger mellan teckensnitt och bakgrund.
Mindre förbättringar.
Använder Qt 5.14.2, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.2, kompilerad med GCC 5.4.0 (Linux)

12:42 9 April 2020
Version 0.18.4
Uppdaterat teckensnitt.
Uppdaterad svensk översättning.
Ny bakgrundsfärg.
Använder Qt 5.14.2, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.2, kompilerad med GCC 5.4.0 (Linux)

01:09 3 April 2020
Version 0.18.3
Uppdaterad svtplay-dl version 2.4-35-g81cb18f
Använder Qt 5.14.2, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.2, kompilerad med GCC 5.4.0 (Linux)

13:49 2 Februari 2020
Version 0.18.2
Uppdaterad svtplay-dl version 2.4-31-g25d3105
Uppdaterad FFmpeg version 4.2.2
Använder Qt 5.14.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.0, kompilerad med GCC 5.4.0 (Linux)

12:35 31 December 2019
Version 0.18.1
Kontrollera att svtplay-dl och ffmpeg finns med och hittas.
Ingen uppgift om svtplay-dl-versionen i dialogrutan "Om".
Använder Qt 5.14.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.0, kompilerad med GCC 5.4.0 (Linux).

16:23 20 December 2019
Version 0.18.0
Efter "Lista alla avsnitt", välj och ladda ner valda avsnitt.
Använder Qt 5.14.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.0, kompilerad med GCC 5.4.0 (Linux).

22:55 25 November 2019
Version 0.17.0
Förbättrad visning av sökresultat.
Möjlighet att använda en förvald folder för nedladdning.
Möjlighet att använda en förvald folder dit mediafilerna och undertexterna kopieras.
Möjlighet att söka efter och lista alla episoder i en serie.
Möjlighet att ladda ner alla episoder på en gång.
Möjligt att avbryta pågående nedladdning. (Windows)
Möjlighet att radera alla inställningsfiler.
Förbättrad hantering av undertexter.
Många buggfixar.
Rättat felstavningar.
"Avinstalleraren" borttagen från Windows, Portable version.
Använder Qt 5.13.2, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.2, kompilerad med GCC 5.4.0 (Linux).

11:01 20 October 2019
Version 0.16.0
Statisk FFmpeg version 4.2.1 (Linux and Windows).
Uppdaterad svtplay-dl version 2.4-29-gc59a305
Tydligare meddelande från svtplay-dl om sökresultatet.
Mindre buggfixar.
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

21:49 11 Oktober 2019
Version 0.15.0
Bugg: "/" sist i adressen tas nu bort.
Menyval för att avinstallera (Windows).
Uppdaterad svtplay-dl version 2.4-26-g5dcc899 (Linux and Windows).
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

21:24 16 September 2019
Version 0.14.0
Frågetecken i sökadressen tas bort.
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

19:53 16 September 2019
Version 0.13.9
Uppdaterad svtplay-dl version 2.4-20-g0826b8f
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

23:24 13 September 2019
Version 0.13.8
Uppdaterad svtplay-dl version 2.4-6-gce9bfd3
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

21:59 11 September 2019
Version 0.13.7
Uppdaterad svtplay-dl version 2.4-2-g5466853
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

09:19 7 September 2019
Version 0.13.6
Uppdaterad svtplay-dl version 2.3-23-gbc15c69
Uppdaterad ffmpeg version 4.2 (Windows).
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).
22:17 2 September 2019

Version 0.13.5
Uppdaterad svtplay-dl version 2.2-5-gd1904b2 (Linux).
Uppdaterad svtplay-dl version 2.2-7-g62cbf8a (Windows).
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

00:06 1 September 2019
Version 0.13.4
Uppdaterad svtplay-dl version 2.2-2-g838b3ec (Linux).
Uppdaterad svtplay-dl version 2.2-14-gc77a4a5 (Windows).
Mindre bugg. Visar inte "Söker..." efter misslyckad sökning.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

18:03 28 Augusti 2019
Version 0.13.3
Uppdaterad svtplay-dl version 2.2-1-g9146d0d
Nedladdningslänk pekar till gitlab.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

09:48 24 Augusti 2019
Version 0.13.2
Uppdaterad svtplay-dl version 2.1-65-gb64dbf3
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

17:43 21 Augusti 2019
Version 0.13.1
Bug fix, Nu fungerar: "Kontrollera automatiskt efter uppdateringar när programmet startar"
Uppdaterad svtplay-dl version 2.1-57-gf429cfc
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

17:59 17 Augusti 2019
Version 0.13.0
Information om vad som uppdateras när programmet söker efter uppdateringar.
Förbättrad "Om".
Flyttade "Lösenord" från "Arkiv" -menyn till "Betal-TV".
Bättre statusmeddelanden.
Bekräftelse på att videoströmmen har laddats ner
Visa antal nedladdade strömmar.
Operativsystemberoende visning av sökväg.
Mindre bug fix.
Architecture instruction set 64-bit.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

12:07 6 Augusti 2019
Version 0.12.0
Uppdaterad svtplay-dl version 2.1-56-gbe6005e (Windows).
Nedgradderad ffmpeg (Windows, version 4.1.4, stabil).
Skapa automatiska mappar, om det behövs. (Samma videoström i olika kvaliteter).
Möjlighet att se mer information från svtplay-dl.
Lägg till status-tips text.
Arkitekturens instruktionsuppsättning 64-bit.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

16:32 30 juli 2019
Version 0.11.0
Använder Qt 5.13.0, kompilerad med  MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med  GCC 5.4.0 (Linux).

22:41 30 juli 2019
Version 0.11.0 RC 1
Förbättrad hjälp och manual.
Redigerbar Download list.
Möjlighet att spara samma stream  i olika storlekar.
Möjlighet att ange användarnamn och lösenord. För betalda streamingtjänster.
Ersätter gammal kod med modernare.
Använder QSettings.
Många bugfixar.
Använder Qt 5.13.0, kompilerad med  MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med  GCC 5.4.0 (Linux).

14:39 5 juli 2019
Version 0.10.7
Flera buggfixar. Fungerar bättre att ladda ner en enskild fil.
Dublikat i valet mellan olika videokvaliteer borttagna.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

14:59 29 juni 2019
Version 0.10.5
Uppdaterad svtplay-dl version 2.1-53-gd33186e
Uppdaterad ffmpeg (version N-94129-g098ab93257).
Arkitekturens instruktionsuppsättning: 64-bit.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

21:29 13 februari 2018
Version 0.10.3
Uppdaterad svtplay-dl version 1.9.7
Arkitekturens instruktionsuppsättning: 64-bit.
Använder Qt 5.10.0, kompilerad med MSVC++ 15.5 (MSVS 2017).

15:41 9 augusti 2017
Version 0.10.1
Kryssruta för att söka efter, ladda och
infoga undertexter i videofilen.
Uppdaterad ffmpeg version 3.3.3
Dynamiskt användargränssnitt.
Arkitekturens instruktionsuppsättning: 64-bit.
Använder Qt 5.9.1, kompilerad med MSVC++ 15.0 (MSVS 2017).

01:19 26 juli 2017
Version 0.10.0
Combo Box för att välja kvalitet.
Arkitekturens instruktionsuppsättning: 64-bit.
Använder Qt 5.9.1, kompilerad med MSVC++ 15.0 (MSVS 2017).

15:48 16 juli 2017
Version 0.9.4
Uppdaterad ffmpeg version 3.3.2
Arkitekturens instruktionsuppsättning: 64-bit.
Använder Qt 5.9.1, kompilerad med MSVC++ 15.0 (MSVS 2017).

09:38 7 maj 2017
Version 0.9.3
Uppdaterad svtplay-dl version 1.9.4
Uppdaterad ffmpeg version 3.2.4
Visar versionshistoriken.

16:33 27 january 2017
Version 0.9.2
Uppdaterad svtplay-dl (version 1.9.1).

18:00 14 januari 2017
Version 0.9.1
Uppdaterade svtplay-dl och ffmpeg
Tysk översättning.
Mindre förbättringar.

01:55 30 oktober 2016
Release version 0.9.0
