//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include "info.h"
#ifdef Q_OS_LINUX
void Newprg::firstRun()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("FirstRun");
    bool firstrun = settings.value("firstrun", true).toBool();
    settings.setValue("firstrun", firstrun);
    settings.endGroup();
    settings.sync();

    if(!firstrun) {
        return;
    }

    QString inifile = settings.fileName();
    QFileInfo fi(inifile);
    QString iconpath = QDir::toNativeSeparators(fi.absolutePath());
    QDir pathDir(iconpath);

    if(pathDir.exists()) {
        iconpath.append(QDir::toNativeSeparators("/streamcapture2.png"));
        const QString pngpath = QDir::toNativeSeparators(QCoreApplication::applicationDirPath() + "/streamcapture2.png");

        if(QFile::exists(iconpath)) {
            QFile::remove(iconpath);
        }

        if(QFile::copy(pngpath, iconpath)) {
            QFile file(iconpath);
            file.setPermissions(QFileDevice::ReadUser | QFileDevice::WriteUser | QFileDevice::ReadOther);
        }
    }

    settings.beginGroup("FirstRun");
    settings.setValue("firstrun", false);
    settings.endGroup();
}
#endif
