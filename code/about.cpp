//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "checkupdate.h"
#include "info.h"
#include "newprg.h"
#include "ui_newprg.h"
void Newprg::about()
{
    auto *information = new Info;
    information->getSystem();
    delete information;
}
void Newprg::versionHistory()
{
    ui->teOut->clear();
    ui->teOut->setReadOnly(true);
    ui->teOut->setAcceptRichText(false);
    ui->actionSaveDownloadList->setEnabled(false);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup(QStringLiteral(u"Language"));
    QString sp = settings.value(QStringLiteral(u"language"), "").toString();
    settings.endGroup();
    QString readme_sp(QStringLiteral(u":/txt/readme_") + sp + QStringLiteral(u".txt"));
    QFile file(readme_sp);

    if(file.exists()) {
        file.open(QIODevice::ReadOnly);
        QTextStream stream(&file);
        stream.setCodec("UTF-8");
        QString content = stream.readAll();
        file.close();
        ui->teOut->setText(content);
        ui->teOut->moveCursor(QTextCursor::Start);
    } else {
        QString readme(QStringLiteral(u":/txt/readme.txt"));
        QFile file(readme);

        if(file.exists()) {
            file.open(QIODevice::ReadOnly);
            QTextStream stream(&file);
            QString content = stream.readAll();
            file.close();
            ui->teOut->setText(content);
            ui->teOut->moveCursor(QTextCursor::Start);
        } else {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("The version history file is not found."));
            msgBox.addButton(QMessageBox::Ok);
            msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
            msgBox.exec();
        }
    }
}

void Newprg::license()
{
    ui->teOut->setReadOnly(true);
    ui->teOut->setAcceptRichText(false);
    ui->actionSaveDownloadList->setEnabled(false);
    QString license(QStringLiteral(u":/txt/license.txt"));
    QFile file(license);

    if(!file.exists()) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("The license file is not found."));
        msgBox.addButton(QMessageBox::Ok);
        msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
        msgBox.exec();
    } else {
        file.open(QIODevice::ReadOnly);
        QTextStream stream(&file);
        QString content = stream.readAll();
        file.close();
        //  ui->teOut->setFontWeight(QFont::Normal);
        ui->teOut->setText(content);
        ui->teOut->moveCursor(QTextCursor::Start);
    }
}

void Newprg::licenseSvtplayDl()
{
    ui->teOut->setReadOnly(true);
    ui->teOut->setAcceptRichText(false);
    ui->actionSaveDownloadList->setEnabled(false);
    QString license_svtplay_dl(QStringLiteral(u":/txt/license_svtplay-dl.txt"));
    QFile file(license_svtplay_dl);

    if(!file.exists()) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("The license file is not found."));
        msgBox.addButton(QMessageBox::Ok);
        msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
        msgBox.exec();
    } else {
        file.open(QIODevice::ReadOnly);
        QTextStream stream(&file);
        QString content = stream.readAll();
        file.close();
        ui->teOut->setText(content);
        ui->teOut->moveCursor(QTextCursor::Start);
    }
}
void Newprg::licenseFfmpeg()
{
    ui->teOut->setReadOnly(true);
    ui->teOut->setAcceptRichText(false);
    ui->actionSaveDownloadList->setEnabled(false);
    QString license_ffmpeg(QStringLiteral(u":/txt/license_FFmpeg.txt"));
    QFile file(license_ffmpeg);

    if(!file.exists()) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("The license file is not found."));
        msgBox.addButton(QMessageBox::Ok);
        msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
        msgBox.exec();
    } else {
        file.open(QIODevice::ReadOnly);
        QTextStream stream(&file);
        QString content = stream.readAll();
        file.close();
        ui->teOut->setText(content);
        ui->teOut->moveCursor(QTextCursor::Start);
    }
}
void Newprg::aboutFfmpeg()
{
    ui->teOut->setReadOnly(true);
    ui->teOut->setAcceptRichText(false);
    ui->actionSaveDownloadList->setEnabled(false);
    QString EXECUTE = selectFfmpeg();

    if(EXECUTE == QStringLiteral(u"NOTFIND")) {
        ui->teOut->setTextColor("red");
        ui->teOut->setText("FFmpeg" +
                           tr(" could not be found. Go to \"Tools\", \"Maintenance "
                              "Tool\" to install ") +
                           "FFmpeg" + ".");
        ui->teOut->append(tr("Or install ") + "FFmpeg" + tr(" in your system."));
        ui->teOut->setTextColor("black");
        return;
    }

    auto *CommProcess = new QProcess(this);
    CommProcess->setProcessChannelMode(QProcess::MergedChannels);
    CommProcess->start(EXECUTE, QStringList() << QStringLiteral(u"--help"));
    CommProcess->waitForFinished(-1);
    QTextStream stream(CommProcess->readAllStandardOutput());
    QString s;
    s.clear();
    QString line;
    line.clear();
    int i = 0;

    while(!stream.atEnd()) {
        line = stream.readLine();
        line = line.simplified();

        if(i != 0) {
            s = s + "<br>" + line;
        } else {
            s = s + line;
        }

        i++;
    }

    ui->teOut->setText(s);
    ui->teOut->moveCursor(QTextCursor::Start);
}
void Newprg::aboutSvtplayDl()
{
    QString s;

    if(ui->actionSvtplayDlSystem->isChecked()) {
        if(findSvtplayDl() == QStringLiteral(u"NOTFIND")) {
            ui->teOut->setTextColor("red");
#ifdef Q_OS_LINUX
            ui->teOut->setText(tr("svtplay-dl is not found in the system path."));
            ui->teOut->append(
                tr("You can use svtplay-dl that comes with streamCapture2."));
#endif
#ifdef Q_OS_WIN
            ui->teOut->setText(tr("svtplay-dl.exe is not found in the system path."));
            ui->teOut->append(
                tr("You can use svtplay-dl.exe that comes with streamCapture2."));
#endif
            ui->teOut->setTextColor("black");
            return;
        }
    } else {
        if(!fileExists(svtplaydl)) {
            ui->teOut->setTextColor("red");
#ifdef Q_OS_WIN

            if(!ui->actionSvtPlayDlManuallySelected->isChecked()) {
                ui->teOut->setText(svtplaydl + tr(" cannot be found. Go to \"Tools\", "
                                                  "\"Maintenance Tool\" to install."));
#endif
#ifdef Q_OS_LINUX

                if(!ui->actionSvtPlayDlManuallySelected->isChecked()) {
                    ui->teOut->setText(svtplaydl + tr(" cannot be found."));
#endif
                } else {
#ifdef Q_OS_LINUX
                    ui->teOut->setText(
                        tr("svtplay-dl cannot be found or is not an executable program. "
                           "Please select svtplay-dl manually."));
#endif
#ifdef Q_OS_WIN
                    ui->teOut->setText(
                        tr("svtplay-dl.exe cannot be found or is not an executable "
                           "program. Please select svtplay-dl.exe manually."));
#endif
                }

                ui->teOut->setTextColor("black");
                return;
            }
        }

        s = "<b>svtplay-dl " + tr("version ") + getSvtplaydlVersion(&svtplaydl) +
            "</b><br><br>";
        ui->teOut->setReadOnly(true);
        ui->teOut->setAcceptRichText(false);
        ui->actionSaveDownloadList->setEnabled(false);
        auto *CommProcess = new QProcess(this);
        CommProcess->setProcessChannelMode(QProcess::MergedChannels);
        CommProcess->start(svtplaydl, QStringList() << QStringLiteral(u"--help"));
        CommProcess->waitForFinished(-1);
        QTextStream stream(CommProcess->readAllStandardOutput());
        QString line;
        line.clear();
        int i = 0;

        while(!stream.atEnd()) {
            line = stream.readLine();
            line = line.simplified();

            if(i != 0) {
                s = s + "<br>" + line;
            } else {
                s = s + line;
            }

            i++;
        }

        ui->teOut->setText(s);
        ui->teOut->moveCursor(QTextCursor::Start);
    }

    bool Newprg::fileExists(QString & path) {
        QFileInfo check_file(path);

        // check if file exists and if yes: Is it really a file and no directory?
        // and is it executable?
        if(check_file.exists() && check_file.isFile() && check_file.isExecutable()) {
            return true;
        } else {
            return false;
        }
    }
    QString Newprg::getSvtplaydlVersion(QString * svtplaydl) {
        auto *CommProcess = new QProcess(nullptr);
        CommProcess->setProcessChannelMode(QProcess::MergedChannels);
        CommProcess->start(*svtplaydl, QStringList() << QStringLiteral(u"--version"));
        CommProcess->waitForFinished(-1);
        QString line;
        QTextStream stream(CommProcess->readAllStandardOutput());

        while(!stream.atEnd()) {
            line = stream.readLine();
        }

        line = line.mid(11);
        return line;
    }
    QString Newprg::selectFfmpeg() {
#ifdef Q_OS_WIN // QT5
        QString EXECUTE = QDir::toNativeSeparators(
                              QCoreApplication::applicationDirPath() + QStringLiteral(u"/ffmpeg.exe"));
#endif
#ifdef Q_OS_LINUX
        QString EXECUTE = QDir::toNativeSeparators(
                              QCoreApplication::applicationDirPath() + QStringLiteral(u"/ffmpeg"));
#endif

        if(fileExists(EXECUTE)) {
            return EXECUTE;
        } else {
            QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
            QString senv = env.value(QStringLiteral(u"PATH"));
#ifdef Q_OS_LINUX
            QStringList slenv = senv.split(QStringLiteral(u":"));
#endif
#ifdef Q_OS_WIN
            QStringList slenv = senv.split(QStringLiteral(u";"));
#endif

            foreach(QString s, slenv) {
#ifdef Q_OS_LINUX
                s.append(QDir::toNativeSeparators(QStringLiteral(u"/ffmpeg")));
#endif
#ifdef Q_OS_WIN
                s.append(QDir::toNativeSeparators(QStringLiteral(u"/ffmpeg.exe")));
#endif

                if(fileExists(s)) {
#ifdef Q_OS_LINUX
                    EXECUTE = QStringLiteral(u"ffmpeg");
#endif
#ifdef Q_OS_WIN
                    EXECUTE = QStringLiteral(u"ffmpeg.exe");
#endif
                    return EXECUTE;
                }
            }
        }

        return QStringLiteral(u"NOTFIND");
    }
