<!DOCTYPE html>
<html lang="en-US">

<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="stilmall.css">
  <title>Manuale streamCapture2</title>


</head>

<body>
  <div id="container">
      <div class="kursiv margintop">Molte grazie a bovirus per la traduzione italiana.</div>
    <div class="language">
      <img src="images/eng/english.png" alt="">
      <a href="index_en_US.html">In English, Please!</a>
    </div>
    <div class="language">
      <img src="images/sv/swedish.png" alt="Swedish">
      <a href="index_sv_SE.html">På svenska, tack!</a>
    </div>
    <p>Risorse internet
      | <a href="http://ceicer.org/streamcapture2/index_eng.php" target="_blank">Sito web programma</a> |
      <a href="https://gitlab.com/posktomten/streamcapture2/wikis/home" target="_blank">Repository sorgenti Gitlab</a> |
    </p>
    <h2>Manuale streamCapture2 versione 0.28</h2>
    <p class="bold">Uso drag and drop</p>
    <img src="images/it/dragenddrop.png" alt="Drag and drop"><br>
    <br>
    <span class="kursiv">Per copiare l'indirizzo trascinalo dalla barra degli indirizzi del browser in streamCapture2 e rilascia .</span>
    <h3>Download di un singolo file</h3>
    <ol>
      <li>Trova la pagina web con lo stream video</li>
      <li>Copia l'indirizzo della pagina web</li>
      <li>Incolla l'indirizzo della pagina web in streamCapture2</li>
      <li>Seleziona &quot;Cerca&quot;</li>
      <li>Scegli la qualità</li>
      <li>Per avviare il download seleziona &quot;Scarica&quot;</li>
    </ol>
    <h3>Download di file multipli</h3>
    <ol>
      <li>Trova la pagina web con lo stream video</li>
      <li>Copia l'indirizzo della pagina web</li>
      <li>Incolla l'indirizzo della pagina web in streamCapture2</li>
      <li>Seleziona &quot;Cerca&quot;</li>
      <li>Scegli la qualità</li>
      <li>Seleziona &quot;Aggiungi ad elenco download&quot; </li>
      <li>Per avviare il download seleziona &quot;Scarica tutto&quot;</li>
    </ol>
    <h3>Download di tutti gli episodi</h3>
    <ol>
      <li>Trova la pagina web con lo stream video</li>
      <li>Copia l'indirizzo web della pagina</li>
      <li>Incolla l'indirizzo&quot; nel programma</li>
      <li>Seleziona &quot;Episodi diversi&quot;, &quot;Elenco di tutti gli stream video&quot;</li>
      <li>Seleziona &quot;Episodi diversi&quot;, &quot;Download diretto di tutti gli stream video della serie attuale (non da elenco download)&quot;</li>
      <li>Il programma selezionerà automaticamente gli stream a qualità più alta</li>
    </ol>
    <h3>Menù del programma</h3>
    <!-- file.png -->
    <img src="images/it/file.png" alt="File">
    <ol>
      <li>Incolla <span class="kursiv">- Incolla (URL) pagina web dagli Appunti.</span></li>
      <li>Cerca <span class="kursiv">- Cerca stream video da scaricare.</span></li>
      <li>Scarica <span class="kursiv">- Download di uno stream video usando il metodo e la qualità video selezionati.</span></li>
      <li>Includi sottotitoli <span class="kursiv">- Seleziona questa opzione se vuoi scaricare anche i sottotitoli.</span></li>
      <li>Stop di tutti i download <span class="kursiv">- Interrompe i download e chiude il programma svtplay-dl (solo per Windows). </span></li>
      <li>Esci <span class="kursiv">- Uscita dal programma. </span></li>
    </ol>
    <!-- recent.png -->
    <img src="images/it/recent.png" alt="Recenti">
    <ol>
      <li>Se fai clic su una delle ricerche precedenti, l'indirizzo verrà copiato nella casella di ricerca.</li>
      <li>Imposta numero ricerche predenti salvate... <span class="kursiv">- Puoi scegliere tra 0 e 50. Se il numero di ricerche supera il valore selezionato, la ricerca più vecchia verrà eliminata.</span></li>
      <li>Rimuovi tutte le ricerche salvate <span class="kursiv">- Tutte le ricerche precedenti salvate verranno eliminate.</span></li>
    </ol>

    <!-- downloadlist.png -->
    <img src="images/it/downloadlist.png" alt="Elenco download">
    <br>
    <ol>
      <li>Visualizza elenco download <span class="kursiv">- Visualizza l'elenco dei flussi video aggiunti per il download.</span></li>
      <li>Aggiungi ad elenco download <span class="kursiv">- Aggiunge uno stream video per il download successivo. È necessario scegliere il metodo e la qualità, nonché se vuoi che i sottotitoli vengano scaricati prima di fare clic. L'elenco verrà salvato automaticamente.</span></li>
      <li>Scarica tutto <span class="kursiv">- Scarica tutti i flussi video presenti nell'elenco con le impostazioni selezionate quando è stato aggiunto il flusso video (qualità, metodo e sottotitoli o non sottotitoli). Se viene scaricato lo stesso flusso video con qualità diverse, verranno create automaticamente le cartelle.</span></li>
      <li>Modifica elenco download (avanzato) <span class="kursiv">- Ad esempio, puoi eliminare un flusso video, modificare l'elenco per caricare i sottotitoli e altro ancora. Modifica l'elenco solo se sei sicuro di come funziona.
</span></li>
      <li>Salva elenco download (avanzato) <span class="kursiv">- Salva le modifiche all'elenco download.</span></li>
      <li>Elimina elenco download <span class="kursiv">- Elimina l'intero elenco download.</span></li>
    </ol>

    <!-- downloadlist_example.png -->
    <img src="images/it/downloadlist_example.png" alt="Esempio elenco download">
    <br>
    <br>
    <span class="kursiv">L'elenco download è un elenco separato da virgole di proprietà usate durante il download dei flussi video salvati nell'elenco download.</span>

    <ol>
      <li>Indirizzo (URL) <span class="kursiv">- Es.: https://www.viafree.se/program/reality/the-real-housewives-of-orange-county/sasong-9/avsnitt-1</span></li>
      <li>Qualità (bitrate) <span class="kursiv">- Es.: 4111</span></li>
      <li>Metodo <span class="kursiv">- Esempio: HLS</span></li>
      <li>Password (nome provider flusso video se viene usata la password, "viafree" se non viene usata la password) <span class="kursiv">- Es.: viafree</span></li>
      <li>Download dei sottotitoli oppure no <span class="kursiv">- Es.: ysub o nsub</span></li>
    </ol>

    <!-- language.png -->
    <img src="images/it/language.png" alt="Lingua">
    <ol>
      <li>Seleziona una delle lingue dell'interfaccia</li>
      <li>Carica file esterno lingua... <span class="kursiv">- Vedi le <a class="kursiv" href="https://gitlab.com/posktomten/streamcapture2/-/wikis/Translate" target="_blank">istruzioni</a> su come tradurre streamCapture2.</span></li>
    </ol>

    <!-- tools.png -->
    <img src="images/it/tools.png" alt="Strumenti">
    <br>
    <ol>
      <li>Controlla aggiornamenti ad avvio del programma <span class="kursiv">- Se è stata selezionata la casella ogni volta che il programma viene avviato verifica la disponibilità di aggiornamenti.</span></li>
      <li>Crea cartella &quot;metodo_qualità&quot; <span class="kursiv">- Se è stata selezionata la casella di controllo, viene creata una cartella che ha per nomi di file e sottocartelle selezionate metodo e qualità.
          Per esempio, &quot;bolibompa-baby-visor-season-6-sma-frogs/dash_3039&quot;. Ciò avviene automaticamente se scarichi lo stesso video con qualità diverse.</span></li>
      <li>Visualizza altro <span class="kursiv">- Visualizza informazioni aggiuntive su svtplay-dl.</span></li>
      <li>Seleziona percorso predefinito download... <span class="kursiv">- Il percorso predefinito dove salvare i flusso video.</span></li>
      <li>Download nel percorso predefinito <span class="kursiv">- Il download inizia immediatamente e i flussi video vengono salvati nel percorso predefinito.</span></li>
      <li>Seleziona percorso in cui copiare... <span class="kursiv">- Il file multimediale completo può essere copiato automaticamente nella cartella selezionata. Dopo che tutti i file, l'audio, i sottotitoli e i video sono stati scaricati e uniti, viene copiato il file MPEG4 finale.</span></li>
      <li>Copia nel percorso selezionato <span class="kursiv">- Copia i file video nel percorso predefinito per la copia.</span></li>
      <li>Usa svtplay-dl dal percorso di sistema <span class="kursiv">- Usa svtplay-dl trovato nel percorso di sistema.</span></li>
      <li>Usa la versione stabile più recente di svtplay-dl <span class="kursiv">- Usa la versione stabile più recente di svtplay-dl.</span></li>
      <li>Usa la versione beta più recente di svtplay-dl <span class="kursiv">- Usa la versione beta più recente di svtplay-dl.</span></li>
      <li>Seleziona svtplay-dl... <span class="kursiv">- Trova e seleziona svtplay-dl nel sistema.</span></li>
      <li>Usa svtplay-dl selezionato... <span class="kursiv">- Usa la versione di svtplay-dl selezionata.</span></li>
      <li>Non visualizzare le notifiche <span class="kursiv">- Con questa opzione non abilitata potresti ricevere un messaggio sullo schermo quando il flusso video o i flussi video vengono scaricati.</span></li>
      <li>Collegamento sul desktop <span class="kursiv">- Crea collegamento di streamCapture2 sul desktop.</span></li>
      <li>Collegamento menu applicazioni <span class="kursiv">- Crea collegamento di streamCapture2 nel gruppo programmi. Non è presente se usi la versione Windows portatile. Il programma di installazione crea invece un collegamento.
</span></li>
      <li>Seleziona font... <span class="kursiv">- Seleziona font, dimensione e tipo font.</span></li>
      <li>Strumento manutenzione... <span class="kursiv">- (Windows) Aggiunge o rimuove componenti, aggiorna componenti o rimuove tutti i componenti (disinstallazione). Valido solo se streamCapture2 è installato.</span></li>
      <li>Aggiorna... <span class="kursiv">- (Linux) Aggiorna AppImage Linux.</span></li>
      </ul>Open the configuration file with system text editor
      </li>
      <li>Modifica impostazioni (avanzate) <span class="kursiv">Apre il file di configurazione con l'editor testo di sistema.</span></li>
      <li>Elimina tutte le impostazioni ed esci <span class="kursiv">- Elimina tutti i file di configurazione, le ricerche salvate e l'elenco dei download.</span></li>
	  <!-- NEW -->
	  <li>Download svtplay-dl... <span class="kursiv">Possibility to download and unpack compressed archives with svtplay-dl from bin.ceicer.com. They are compiled from the source code at https://github.com/spaam/svtplay-dl.</span></li>

    </ol>
    <img src="images/it/maintenancetool.png" alt="Strumento manutenzione">
    <br>
    <br>
    <span class="kursiv">Strumento manutenzione</span>
    <br>
    <br>
    <!-- paytv.png -->
    <img src="images/it/paytv.png" alt="TV a pagamento">
    <ol>
      <li>Password <span class="kursiv">- Inserisci la password per scaricare da un servizio di streaming a pagamento. Puoi scegliere di salvare la password per evitare di digitarla ogni volta (TV a pagamento-&gt; Provider -&gt; Modifica).</span></li>
      <li>Crea nuovo provider servizi <span class="kursiv">- Puoi aggiungere un servizio di streaming a pagamento. Inserisci il nome del provider e il nome utente. Puoi fare in modo che il programma salvi la password o scegliere di inserirla ogni volta che scarichi.</span></li>
      <li>Fai clic su un provider servizi esistente <span class="kursiv">- Scegli tu se eliminare il servizio di streaming o modificare il nome del servizio, il nome utente o la password.</span></li>
    </ol>

    <!-- st.png -->
    <img src="images/it/st.png" alt="Episodi diversi">
    <ol>
       <li>Imposta cookie 'st' <span class="kursiv">- <a href="https://youtu.be/XQ5LzMMgIa8">Cerca nel browser il cookie 'st'</a> per poterlo scaricare in certe pagine dove è richiesto. Fai lic e incollalo.</span></li> 
       <li>Ricerca+ <span class="kursiv">- Fai clic su uno dei provider di streaming che richiedono il cookie "st" per modificare le impostazioni.</span></li>      
    </ol>
    <!-- severalepisodes.png -->
    <img src="images/it/severalepisodes.png" alt="Episodi diversi">
    <ol>
      <li>Elenco di tutti gli stream video <span class="kursiv">- Elenca tutti i flussi video che appartengono alla stessa serie TV.</span>
      <li>Aggiungi tutti gli stream video all'elenco download <span class="kursiv">- Aggiunge all'elenco download tutti i flussi video che appartengono alla serie TV attuale.</span></li>
      <li>Download diretto tutti gli episdodi nella serie attuale (non da elenco download) <span class="kursiv">- Cerca di trovare e scaricare tutti i flussi video che appartengono alla serie TV attuale senza aggiungerli all'elenco download. Vengono selezionate automaticamente la qualità migliore e la dimensione massima del file.</span>
      </li>
    </ol>

    <!-- help.png -->
    <img src="images/it/help.png" alt="Guida">
    <ol>
      <li>Guida in linea... <span class="kursiv">- Visualizza questa guida in linea.</span></li>
      <li>Controllo aggiornamenti... <span class="kursiv">Controlla ed avvisa se è disponibile una nuova versione di streamCapture2. Richiede una connessione a Internet.</span></li>
      <li>Cronologia versioni... <span class="kursiv">- Visualizza la cronologia delle diverse versioni di streamCapture2.</span></li>
      <li>Info su streamCapture2... <span class="kursiv">- Info tecniche e sulla versione di streamCapture2.</span></li>
      <li>Info su svtplay-dl... <span class="kursiv">- Informazioni versione e opzioni configurazione di svtplay-dl. Seleziona svtplay-dl nel menu &quot;Strumenti&quot;. Se non hai effettuato una scelta, verrà selezionato &quot;svtplay-dl stabile&quot;.</span></li>
      <li>Visita svtplay-dl forum... <span class="kursiv">- Collegamento al forum di svtplay-dl. Apre il collegamento nel browser web.</span></li>
      <li>Info su FFmpeg... <span class="kursiv">Info versione e opzioni di configurazione di FFmpeg.</span></li>
      <li>Licenza streamCapture2... <span class="kursiv">- Visualizza le info sulla licenza di streamCapture2.</span></li>
      <li>Licenza svtplay-dl... <span class="kursiv">- Visualizza le info sulla licenza di svtplay-dl.</span></li>
      <li>Licenza FFmpeg... <span class="kursiv">- Visualizza le info sulla licenza di FFmpeg.</span></li>
    </ol>

    <!-- listboxkvalitet.png -->
    <img src="images/it/listboxkvalitet.png" alt="Elenco qualità selezionabili">
    <ol>
      <li>Scegli la qualità <span class="kursiv">- Fai clic sulla casella di riepilogo per selezionare la qualità. Qui è selezionato "2354 HLS". Un bitrate più alto fornisce una migliore qualità e un file più grande da scaricare.</span></li>
      <li>Password <span class="kursiv">- Fai clic sulla casella di riepilogo per selezionare "- Nessuna password" o seleziona un servizio di streaming che richiede una password. Se lasci che il programma salvi le tue password, puoi avere diversi servizi di streaming nell'elenco download. Il programma selezionerà automaticamente la password corretta.</span></li>
    </ol>
    <div class="ruta">
        <p class="bold">Info su FFmpeg</p>
        <p>streamCapture2 usa principalmente FFmpeg fornito con streamCapture2. Se manca FFmpeg, streamCapture cerca FFmpeg nel percorso di sistema. È quindi possibile usare una versione di FFmpeg precedentemente installata.</p>
    </div>
<br>
  </div>
  <!-- container -->
</body>
</html>
