 # streamCapture2

http://ceicer.org/streamcapture2/index_eng.php<br>
<h1>Read more and download: [**Wiki**](https://gitlab.com/posktomten/streamcapture2/wikis/home)</h1>
&copy; Coppyright Ingemar Ceicer GPL v3<br><br>

@posktomten

A program to save streaming video to your computer. A graphical shell for the command line program svtplay-dl
The program works with Linux, Windows and MacOS X. The program is written in C++ and uses Qt5 graphic library. There is a setup program for Windows and Linux. AppImage is available for Linux and Portable version for Windows. No installation required.

[Tested](https://gitlab.com/posktomten/streamcapture2/-/wikis/Home) with several operating systems.

The program can handle three languages English and a litle Germen.

A [translation](https://gitlab.com/posktomten/streamcapture2/-/wikis/Translate) into additional languages would be appreciated!

# streamCapture2 is a graphical shell for the command line program svtplay-dl.
https://svtplay-dl.se/


FFmpeg is used in turn by svtplay-dl

https://www.ffmpeg.org/

streamCapture2 uses the binary libraries [libzsyncupdateappimage](https://gitlab.com/posktomten/libzsyncupdateappimage), [libselectfont](https://gitlab.com/posktomten/libselectfont), [libcheckforupdates](https://gitlab.com/posktomten/libcheckforupdates) and [downloadunpack](https://gitlab.com/posktomten/downloadunpack)<br>

streamCapture2 is designed to work primarily on

```
aftonbladet.se
bambuser.com
comedycentral.se
di.se
dn.se
dplay.se
dr.dk
efn.se
expressen.se
hbo.com
kanal9play.se
nickelodeon.nl
nickelodeon.no
nickelodeon.se
nrk.no
oppetarkiv.se
ruv.is
svd.se
sverigesradio.se
svtplay.se
viafree.se (former tv3play.se, tv6play.se, tv8play.se, tv10play.se)
viafree.dk (former tv3play.dk)
viafree.no (former tv3play.no, viasat4play.no)
tv3play.ee
tv3play.lt
tv3play.lv
tv4.se
tv4play.se
twitch.tv
ur.se
urplay.se
vg.no
viagame.com


```
