The font "PT"
Designed by Alexandra Korolkova, Olga Umpeleva and Vladimir Yefimov
and released by ParaType in 2009.
These fonts are licensed under the Open Font License.
http://scripts.sil.org/OFL
