<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>SelectFont</name>
    <message>
        <location filename="../selectfont.ui" line="26"/>
        <location filename="../selectfont.cpp" line="26"/>
        <source>Select font</source>
        <translation>Seleziona font</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="189"/>
        <source>Normal</source>
        <translation>Normale</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="170"/>
        <source>Bold</source>
        <translation>Grassettto</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="211"/>
        <source>Italic</source>
        <translation>Corsivo</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="230"/>
        <source>Bold and italic</source>
        <translation>Grassetto/corsivo</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="110"/>
        <source>Default</source>
        <translation>Predefiniti</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="132"/>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="72"/>
        <source>All fonts</source>
        <translation>Tutte le font</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="72"/>
        <source>Monospaced fonts</source>
        <translation>Font monospazio</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="73"/>
        <source>Proportional fonts</source>
        <translation>Font proporzionali</translation>
    </message>
</context>
</TS>
