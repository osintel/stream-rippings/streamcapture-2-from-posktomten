<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>DownloadUnpack</name>
    <message>
        <location filename="downloadunpack.ui" line="19"/>
        <location filename="downloadunpack.ui" line="19"/>
        <location filename="downloadunpack.ui" line="19"/>
        <location filename="downloadunpack.ui" line="19"/>
        <location filename="downloadunpack.ui" line="19"/>
        <source>MainWindow</source>
        <translation>Finestra principale</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="157"/>
        <location filename="downloadunpack.ui" line="218"/>
        <location filename="downloadunpack.ui" line="157"/>
        <location filename="downloadunpack.ui" line="218"/>
        <location filename="downloadunpack.ui" line="157"/>
        <location filename="downloadunpack.ui" line="218"/>
        <location filename="downloadunpack.ui" line="157"/>
        <location filename="downloadunpack.ui" line="218"/>
        <location filename="downloadunpack.ui" line="157"/>
        <location filename="downloadunpack.ui" line="218"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="46"/>
        <location filename="downloadunpack.ui" line="206"/>
        <location filename="downloadunpack.ui" line="46"/>
        <location filename="downloadunpack.ui" line="206"/>
        <location filename="downloadunpack.ui" line="46"/>
        <location filename="downloadunpack.ui" line="206"/>
        <location filename="downloadunpack.ui" line="46"/>
        <location filename="downloadunpack.ui" line="206"/>
        <location filename="downloadunpack.ui" line="46"/>
        <location filename="downloadunpack.ui" line="206"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="176"/>
        <location filename="downloadunpack.ui" line="176"/>
        <location filename="downloadunpack.ui" line="176"/>
        <location filename="downloadunpack.ui" line="176"/>
        <location filename="downloadunpack.ui" line="176"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="184"/>
        <location filename="downloadunpack.ui" line="184"/>
        <location filename="downloadunpack.ui" line="184"/>
        <location filename="downloadunpack.ui" line="184"/>
        <location filename="downloadunpack.ui" line="184"/>
        <source>&amp;Tools</source>
        <translation>&amp;Strumenti</translation>
    </message>
    <message>
        <source>&amp;Language</source>
        <translation>&amp;Lingua</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="190"/>
        <location filename="downloadunpack.ui" line="190"/>
        <location filename="downloadunpack.ui" line="190"/>
        <location filename="downloadunpack.ui" line="190"/>
        <location filename="downloadunpack.ui" line="190"/>
        <source>&amp;Visit</source>
        <translation>&amp;Visita</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="226"/>
        <location filename="downloadunpack.ui" line="226"/>
        <location filename="downloadunpack.ui" line="226"/>
        <location filename="downloadunpack.ui" line="226"/>
        <location filename="downloadunpack.ui" line="226"/>
        <source>Try to decompress</source>
        <translation>Prova a decomprimere</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="275"/>
        <location filename="downloadunpack.ui" line="275"/>
        <location filename="downloadunpack.ui" line="275"/>
        <location filename="downloadunpack.ui" line="275"/>
        <location filename="downloadunpack.ui" line="275"/>
        <source>Source code...</source>
        <translation>Codice sorgente...</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="283"/>
        <location filename="downloadunpack.ui" line="283"/>
        <location filename="downloadunpack.ui" line="283"/>
        <location filename="downloadunpack.ui" line="283"/>
        <location filename="downloadunpack.ui" line="283"/>
        <source>Binary files...</source>
        <translation>File binari...</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="115"/>
        <location filename="downloadunpack.ui" line="235"/>
        <location filename="downloadunpack.ui" line="115"/>
        <location filename="downloadunpack.ui" line="235"/>
        <location filename="downloadunpack.ui" line="115"/>
        <location filename="downloadunpack.ui" line="235"/>
        <location filename="downloadunpack.ui" line="115"/>
        <location filename="downloadunpack.ui" line="235"/>
        <location filename="downloadunpack.ui" line="115"/>
        <location filename="downloadunpack.ui" line="235"/>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="238"/>
        <location filename="downloadunpack.ui" line="238"/>
        <location filename="downloadunpack.ui" line="238"/>
        <location filename="downloadunpack.ui" line="238"/>
        <location filename="downloadunpack.ui" line="238"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="247"/>
        <location filename="downloadunpack.ui" line="247"/>
        <location filename="downloadunpack.ui" line="247"/>
        <location filename="downloadunpack.ui" line="247"/>
        <location filename="downloadunpack.ui" line="247"/>
        <source>English</source>
        <translation>Inglese</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="256"/>
        <location filename="downloadunpack.ui" line="256"/>
        <location filename="downloadunpack.ui" line="256"/>
        <location filename="downloadunpack.ui" line="256"/>
        <location filename="downloadunpack.ui" line="256"/>
        <source>Swedish</source>
        <translation>Svedese</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="261"/>
        <location filename="downloadunpack.ui" line="261"/>
        <location filename="downloadunpack.ui" line="261"/>
        <location filename="downloadunpack.ui" line="261"/>
        <location filename="downloadunpack.ui" line="261"/>
        <source>Load external language file...</source>
        <translation>Carica file lingua esterno...</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="270"/>
        <location filename="downloadunpack.ui" line="270"/>
        <location filename="downloadunpack.ui" line="270"/>
        <location filename="downloadunpack.ui" line="270"/>
        <location filename="downloadunpack.ui" line="270"/>
        <source>Italian</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="36"/>
        <location filename="downloadunpack.cpp" line="36"/>
        <location filename="downloadunpack.cpp" line="36"/>
        <location filename="downloadunpack.cpp" line="36"/>
        <location filename="downloadunpack.cpp" line="36"/>
        <source>svtplay-dl from bin.ceicer.com</source>
        <translation>svtplay-dl (bin.ceicer.com)</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="64"/>
        <location filename="downloadunpack.cpp" line="64"/>
        <location filename="downloadunpack.cpp" line="64"/>
        <location filename="downloadunpack.cpp" line="64"/>
        <location filename="downloadunpack.cpp" line="64"/>
        <source>Uses</source>
        <translation>Usa</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="104"/>
        <location filename="downloadunpack.cpp" line="104"/>
        <location filename="downloadunpack.cpp" line="104"/>
        <location filename="downloadunpack.cpp" line="104"/>
        <location filename="downloadunpack.cpp" line="104"/>
        <source>Is not writable. Cancels</source>
        <translation>Non è scrivibile. Annulla</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="133"/>
        <location filename="downloadunpack.cpp" line="133"/>
        <location filename="downloadunpack.cpp" line="133"/>
        <location filename="downloadunpack.cpp" line="133"/>
        <location filename="downloadunpack.cpp" line="133"/>
        <source>to</source>
        <translation>in</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="133"/>
        <location filename="downloadunpack.cpp" line="135"/>
        <location filename="downloadunpack.cpp" line="133"/>
        <location filename="downloadunpack.cpp" line="135"/>
        <location filename="downloadunpack.cpp" line="133"/>
        <location filename="downloadunpack.cpp" line="135"/>
        <location filename="downloadunpack.cpp" line="133"/>
        <location filename="downloadunpack.cpp" line="135"/>
        <location filename="downloadunpack.cpp" line="133"/>
        <location filename="downloadunpack.cpp" line="135"/>
        <source>Check your file permissions and antivirus software.</source>
        <translation>Verifica permessi file e software antivirus.</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="133"/>
        <location filename="downloadunpack.cpp" line="133"/>
        <location filename="downloadunpack.cpp" line="133"/>
        <location filename="downloadunpack.cpp" line="133"/>
        <location filename="downloadunpack.cpp" line="133"/>
        <source>Can not save</source>
        <translation>Impossibile salvare</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="135"/>
        <location filename="downloadunpack.cpp" line="135"/>
        <location filename="downloadunpack.cpp" line="135"/>
        <location filename="downloadunpack.cpp" line="135"/>
        <location filename="downloadunpack.cpp" line="135"/>
        <source>Can not save to</source>
        <translation>Impossibile salvare in</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="138"/>
        <location filename="downloadunpack.cpp" line="138"/>
        <location filename="downloadunpack.cpp" line="138"/>
        <location filename="downloadunpack.cpp" line="138"/>
        <location filename="downloadunpack.cpp" line="138"/>
        <source>is downloaded to</source>
        <translation>è stato scaricato in</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="153"/>
        <location filename="downloadunpack.cpp" line="153"/>
        <location filename="downloadunpack.cpp" line="153"/>
        <location filename="downloadunpack.cpp" line="153"/>
        <location filename="downloadunpack.cpp" line="153"/>
        <source>Starting to decompress</source>
        <translation>Avvio decompressione</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="170"/>
        <location filename="downloadunpack.cpp" line="170"/>
        <location filename="downloadunpack.cpp" line="170"/>
        <location filename="downloadunpack.cpp" line="170"/>
        <location filename="downloadunpack.cpp" line="170"/>
        <source>is decompressed to</source>
        <translation>è stato decomrpesso in</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="173"/>
        <location filename="downloadunpack.cpp" line="205"/>
        <location filename="downloadunpack.cpp" line="173"/>
        <location filename="downloadunpack.cpp" line="205"/>
        <location filename="downloadunpack.cpp" line="173"/>
        <location filename="downloadunpack.cpp" line="205"/>
        <location filename="downloadunpack.cpp" line="173"/>
        <location filename="downloadunpack.cpp" line="205"/>
        <location filename="downloadunpack.cpp" line="173"/>
        <location filename="downloadunpack.cpp" line="205"/>
        <source>Failed to decompress.</source>
        <translation>Decompressione fallita.</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="239"/>
        <location filename="downloadunpack.cpp" line="239"/>
        <location filename="downloadunpack.cpp" line="239"/>
        <location filename="downloadunpack.cpp" line="239"/>
        <location filename="downloadunpack.cpp" line="239"/>
        <source>files</source>
        <translation>file</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="241"/>
        <location filename="downloadunpack.cpp" line="241"/>
        <location filename="downloadunpack.cpp" line="241"/>
        <location filename="downloadunpack.cpp" line="241"/>
        <location filename="downloadunpack.cpp" line="241"/>
        <source>file</source>
        <translation>file</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="filedialog.cpp" line="35"/>
        <location filename="filedialog.cpp" line="35"/>
        <location filename="filedialog.cpp" line="35"/>
        <location filename="filedialog.cpp" line="35"/>
        <location filename="filedialog.cpp" line="35"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Download svtplay-dl to</source>
        <translation>Download svtplay-dl in</translation>
    </message>
    <message>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation>Per usare la nuova lingua il programma deve essere riavviato.</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation>Riavvia</translation>
    </message>
    <message>
        <source>Open your language file</source>
        <translation>Apri file lingua</translation>
    </message>
    <message>
        <source>Compiled language file (*.qm)</source>
        <translation>File lingua compilato (*.qm)</translation>
    </message>
    <message>
        <location filename="filedialog.cpp" line="34"/>
        <location filename="filedialog.cpp" line="34"/>
        <location filename="filedialog.cpp" line="34"/>
        <location filename="filedialog.cpp" line="34"/>
        <location filename="filedialog.cpp" line="34"/>
        <source>Choose</source>
        <translation>Scegli</translation>
    </message>
    <message>
        <location filename="filedialog.cpp" line="36"/>
        <location filename="filedialog.cpp" line="36"/>
        <location filename="filedialog.cpp" line="36"/>
        <location filename="filedialog.cpp" line="36"/>
        <location filename="filedialog.cpp" line="36"/>
        <source>Save svtplay-dl in directory</source>
        <translation>Salva svtplay-dll nella cartella</translation>
    </message>
</context>
</TS>
