<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT" sourcelanguage="it_IT">
<context>
    <name>CheckUpdate</name>
    <message>
        <location filename="../checkupdate.cpp" line="51"/>
        <location filename="../checkupdate.cpp" line="131"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Nessuna connessione internet disponibile.
Verifica le impostazioni internet ed il firewall.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="53"/>
        <location filename="../checkupdate.cpp" line="72"/>
        <location filename="../checkupdate.cpp" line="82"/>
        <location filename="../checkupdate.cpp" line="92"/>
        <location filename="../checkupdate.cpp" line="133"/>
        <location filename="../checkupdate.cpp" line="151"/>
        <location filename="../checkupdate.cpp" line="223"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="69"/>
        <source>Your version of </source>
        <translation>La versione di </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="70"/>
        <source> is newer than the latest official version. </source>
        <translation> è più recente della versione ufficiale più recente. </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="80"/>
        <source>Your version of</source>
        <translation>La versione di</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="80"/>
        <source>is the same version as the latest official version.</source>
        <translation>è la stessa versione della versione ufficiale più recente.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="90"/>
        <source>There was an error when the version was checked.</source>
        <translation>Si è verificato un errore durante il controllo versione.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="149"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Si è verificato un errore durante il controllo versione.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="195"/>
        <source>Updates:</source>
        <translation>Aggiornamenti:</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="218"/>
        <source>There is a new version of </source>
        <translation>È dispoibile una nuova versione di </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="220"/>
        <source>Latest version: </source>
        <translation>Versione aggiornata: </translation>
    </message>
</context>
</TS>
