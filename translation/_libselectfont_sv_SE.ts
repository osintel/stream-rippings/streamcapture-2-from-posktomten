<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>SelectFont</name>
    <message>
        <location filename="../selectfont.ui" line="26"/>
        <location filename="../selectfont.cpp" line="26"/>
        <source>Select font</source>
        <translation>Välj teckensnitt</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="189"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="170"/>
        <source>Bold</source>
        <translation>Fet</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="211"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="230"/>
        <source>Bold and italic</source>
        <translation>Fet och kursiv</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="110"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="132"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="72"/>
        <source>All fonts</source>
        <translation>Alla teckensnitt</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="72"/>
        <source>Monospaced fonts</source>
        <translation>Teckensnitt med fast breddsteg</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="73"/>
        <source>Proportional fonts</source>
        <translation>Proportionella teckensnitt</translation>
    </message>
</context>
</TS>
